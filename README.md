# LAMP-SSL-StandAlone

docker-compose สำหรับสร้าง `LAMP` พร้อม SSL จาก let's encrypt
(โดยไม่ได้ใช้คู่กับ nginx + acme companion)

# startup
- รอบแรกให้ศึกษา `_create-ssl-example.run` โดยกำหนดค่าต่างๆ ให้ครบถ้วน
ซึ่งจะเป็นการไปเรียก certbot มาสร้าง certificate key ครั้งแรกนี้จะใช้ port 80
สามารถเรียก docker-compose โดยใช้ config `docker-compose-nossl.yml`

- หลังจากทำเสร็จแล้ว ต่อไปก็จะสามารถใช้ SSL (port 443) ได้แล้ว ก็ให้เรียก
docker-compose โดยใช้ config `docker-compose-ssl.yml` ได้แล้ว

# การ renew certbot
- สามารถตั้ง contrab เอาไว้ โดยดูตัวอย่างที่ `_renew-ssl-example.run`


`TKVR`
